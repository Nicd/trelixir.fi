pub const url = "https://trelixir.fi"

pub const fediverse_url = "https://social.ahlcode.fi/@trelixir"

pub const fediverse_rss_url = "https://social.ahlcode.fi/@trelixir.rss"

pub const fediverse = "@trelixir@social.ahlcode.fi"
