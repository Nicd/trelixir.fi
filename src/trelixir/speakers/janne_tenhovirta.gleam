import lustre/element/html.{a}
import lustre/element.{text}
import lustre/attribute.{href}
import trelixir/events/types

pub fn data() {
  types.Speaker(
    name: "Janne Tenhovirta",
    img: "jannenaama.jpg",
    description: [
      text(
        "Janne mainly gets his hands dirty in .NET backend development at his job with ",
      ),
      a([href("http://headpower.fi")], [
        text("Cybersoft, a subsidiary of Headpower"),
      ]),
      text(
        ". He feels they get dirtier when fiddling with the frontend. He first learned of Elixir's existence in 2015 and started to fool around with it in 2016.",
      ),
    ],
    company: types.Company(name: "Cybersoft"),
  )
}
