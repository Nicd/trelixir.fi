import lustre/element/html.{a}
import lustre/element.{text}
import lustre/attribute.{href}
import trelixir/events/types

pub fn data() {
  types.Speaker(
    name: "Mikko Ahlroth",
    img: "mikkonaama2.webp",
    description: [
      text("Mikko works primarily as a full stack developer at "),
      a([href("https://vincit.com/")], [text("Vincit")]),
      text(
        ", but on his free time has dabbled with Elixir since 2013, more recently Gleam.He has published ",
      ),
      a([href("https://hex.pm/users/nicd")], [text("some packages")]),
      text(" on hex.pm and runs "),
      a([href("https://codestats.net/")], [text("Code::Stats")]),
      text(" on Phoenix."),
    ],
    company: types.Company(name: "Vincit Oyj"),
  )
}
