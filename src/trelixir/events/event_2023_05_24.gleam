import lustre/element/html.{a, p, sup, time}
import lustre/element.{text}
import lustre/attribute.{attribute, href, target}
import trelixir/events/types
import trelixir/speakers/mikko_ahlroth
import trelixir/tpl/signup_closed

pub const file = "event-2023-05-24"

pub fn data() {
  types.Event(
    title: "Event 24.5.2023",
    file: file,
    host_name: "Vincit",
    host_url: "https://vincit.com/",
    description: [
      p([], [
        text("This event was on "),
        time([attribute("datetime", "2023-05-24")], [
          text("Wednesday 24"),
          sup([], [text("th")]),
          text(" of May"),
        ]),
        text(" at the "),
        a(
          [
            href(
              "https://asiointi.maanmittauslaitos.fi/karttapaikka/?lang=fi&share=customMarker&n=6822019.623407671&e=328371.01345323544&title=Vincit%20City%20Centre&desc=&zoom=10&layers=W3siaWQiOjIsIm9wYWNpdHkiOjEwMH1d-z",
            ),
            target("_blank"),
          ],
          [text("Vincit city centre office")],
        ),
        text(", "),
        a(
          [
            href(
              "https://technopolisglobal.com/office-spaces/tampere/yliopistonrinne/directions/",
            ),
            target("_blank"),
          ],
          [text("Technopolis Yliopistonrinne")],
        ),
        text(", Kalevantie 2, Tampere."),
      ]),
    ],
    signup_info: signup_closed.view(),
    talks: [
      types.Talk(
        title: "UTC Is Not Your Saviour",
        speaker: mikko_ahlroth.data(),
        slides: [
          types.Slide(
            title: "Presentation",
            link: "https://www.jottacloud.com/s/087f20c9752702c479da591b46a2ea55f04",
          ),
        ],
      ),
      types.Talk(
        title: "MyProjectOS – A Viewpoint to Your Project",
        speaker: mikko_ahlroth.data(),
        slides: [
          types.Slide(
            title: "Presentation",
            link: "https://www.jottacloud.com/s/0871106f282fa4d465fa35cf6833015942f",
          ),
        ],
      ),
    ],
  )
}
