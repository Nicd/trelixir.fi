import lustre/element/html.{a, p, sup, time}
import lustre/element.{text}
import lustre/attribute.{attribute, href, target}
import trelixir/events/types
import trelixir/speakers/mikko_ahlroth
import trelixir/speakers/janne_tenhovirta
import trelixir/tpl/signup_closed

pub const file = "event-2017-09-21"

pub fn data() {
  types.Event(
    title: "Event 21.9.2017",
    file: file,
    host_name: "Vincit",
    host_url: "https://vincit.com/",
    description: [
      p([], [
        text("The first TRElixir event was on the "),
        time([attribute("datetime", "2017-09-21")], [
          text("21"),
          sup([], [text("st")]),
          text(" of September, 2017"),
        ]),
        text(" at "),
        a(
          [
            href(
              "https://www.google.fi/maps/place/Vincit/@61.4481532,23.8608591,17z/data=!3m1!4b1!4m5!3m4!1s0x468edfb7a8f2c79f:0x71c0801aebd30465!8m2!3d61.4481532!4d23.8630531",
            ),
            target("_blank"),
          ],
          [text("Vincit premises in Hervanta, Tampere")],
        ),
        text("."),
      ]),
    ],
    signup_info: signup_closed.view(),
    talks: [
      types.Talk(
        title: "Why Elixir matters",
        speaker: mikko_ahlroth.data(),
        slides: [],
      ),
      types.Talk(
        title: "GenServer & Agent",
        speaker: janne_tenhovirta.data(),
        slides: [],
      ),
    ],
  )
}
