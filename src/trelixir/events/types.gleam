import lustre/element.{type Element}

pub type Company {
  NoCompany
  Company(name: String)
}

pub type Speaker {
  Speaker(
    name: String,
    img: String,
    company: Company,
    description: List(Element(Nil)),
  )
}

pub type Slide {
  Slide(title: String, link: String)
}

pub type Talk {
  Placeholder
  Talk(title: String, speaker: Speaker, slides: List(Slide))
}

pub type Event {
  Event(
    title: String,
    description: List(Element(Nil)),
    host_name: String,
    host_url: String,
    signup_info: List(Element(Nil)),
    talks: List(Talk),
    file: String,
  )
}
