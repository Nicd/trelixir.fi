import trelixir/config

pub fn event_url(event_file: String) {
  config.url <> "/" <> event_file <> ".html"
}
