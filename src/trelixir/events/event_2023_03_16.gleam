import lustre/element/html.{a, p, sup, time}
import lustre/element.{text}
import lustre/attribute.{attribute, href, target}
import trelixir/events/types
import trelixir/speakers/mikko_ahlroth
import trelixir/tpl/signup_closed

pub const file = "event-2023-03-16"

pub fn data() {
  types.Event(
    title: "Event 16.3.2023",
    file: file,
    host_name: "Vincit",
    host_url: "https://vincit.com/",
    description: [
      p([], [
        text("This event was on "),
        time([attribute("datetime", "2023-03-16")], [
          text("16"),
          sup([], [text("th")]),
          text(" of March, 2023"),
        ]),
        text(" at the "),
        a(
          [
            href(
              "https://asiointi.maanmittauslaitos.fi/karttapaikka/?lang=fi&share=customMarker&n=6822019.623407671&e=328371.01345323544&title=Vincit%20City%20Centre&desc=&zoom=10&layers=W3siaWQiOjIsIm9wYWNpdHkiOjEwMH1d-z",
            ),
            target("_blank"),
          ],
          [text("Vincit city centre office")],
        ),
        text("."),
      ]),
    ],
    signup_info: signup_closed.view(),
    talks: [
      types.Talk(
        title: "Gleam – Type Safe BEAM",
        speaker: mikko_ahlroth.data(),
        slides: [
          types.Slide(
            title: "Presentation",
            link: "https://docs.google.com/presentation/d/1Jz_eQFemwfjV_0KqG8YXQ9GJQ7rdTAYOStvbN6sUySA/edit?usp=sharing",
          ),
        ],
      ),
      types.Talk(
        title: "Forget config.exs",
        speaker: mikko_ahlroth.data(),
        slides: [
          types.Slide(
            title: "Presentation",
            link: "https://docs.google.com/presentation/d/1KUB3qXolxa_RRXy84NftzpvVuR46f4S4eLqe4ROc5mQ/edit?usp=sharing",
          ),
          types.Slide(
            title: "Blog post about configuration",
            link: "https://blog.nytsoi.net/2021/04/17/elixir-simple-configuration",
          ),
        ],
      ),
    ],
  )
}
