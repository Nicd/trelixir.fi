import gleam/option
import trelixir/events/event_2023_05_24
import trelixir/events/event_2023_03_16
import trelixir/events/event_2017_09_21
import trelixir/config
import trelixir/tpl/index

pub fn render() {
  index.view(
    option.None,
    [event_2023_05_24.data(), event_2023_03_16.data(), event_2017_09_21.data()],
    config.fediverse,
    config.fediverse_url,
  )
}
