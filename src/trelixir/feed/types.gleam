import lustre/element.{type Element}

pub type FeedItem {
  Item(
    title: String,
    link: String,
    guid: String,
    description: List(Element(Nil)),
    pub_date: String,
  )
}
