import lustre/element/html.{a, em, p, sup, time}
import lustre/element.{text}
import lustre/attribute.{attribute, href}
import trelixir/events/event_2023_03_16
import trelixir/events/event_2023_05_24
import trelixir/events/utils
import trelixir/config
import trelixir/feed/types.{Item}

pub fn items() {
  [
    Item(
      title: "TRElixir afterwork place is Plevna",
      link: config.url,
      guid: "7faff268-a9d5-4ef5-9373-acdf3268d772",
      description: [
        p([], [
          text("The place for our gettogether on Monday the 18"),
          sup([], [text("th")]),
          text(" of December at 17.30 is "),
          a([href("https://plevna.fi/english/")], [text("Plevna")]),
          text(
            ". We will have some kind of sign on the table that says TRElixir so that you can find us. If you cannot, please send me a message on IRC or by email, you can find my contact details on ",
          ),
          a([href(config.url)], [text("trelixir.fi")]),
          text("."),
        ]),
        p([], [text("Welcome!")]),
      ],
      pub_date: "Thu, 14 Dec 2023 20:40:00 +0200",
    ),
    Item(
      title: "TRElixir afterwork: 18.12.2023",
      link: config.url,
      guid: "91c0aee9-07ff-4407-b3f8-bc6ed4663591",
      description: [
        p([], [
          text("On Monday the 18"),
          sup([], [text("th")]),
          text(
            " of December at 17.30, we'll have an afterwork gettogether. We haven't decided on the place yet, but it will be a cafe or restaurant in the city centre (follow this RSS feed or ",
          ),
          a([href(config.fediverse_url)], [text(config.fediverse)]),
          text(" for updates)."),
        ]),
        p([], [
          text(
            "There are no talks planned, the agenda is just to get together and socialize.",
          ),
        ]),
      ],
      pub_date: "Fri, 8 Dec 2023 23:45:00 +0200",
    ),
    Item(
      title: "Fediverse presence",
      link: config.url,
      guid: "fc81fe05-d835-4c14-8cfd-175633a085f7",
      description: [
        p([], [
          text("We've set up a fediverse account at "),
          a([href(config.fediverse_url)], [text(config.fediverse)]),
          text(
            ". It'll contain all the latest info and is updated faster than this website. You can follow it from any fediverse instance, and if you're not on the fediverse, you can read its posts via ",
          ),
          a([href(config.fediverse_rss_url)], [text("RSS")]),
        ]),
      ],
      pub_date: "Fri, 8 Dec 2023 23:30:00 +0200",
    ),
    Item(
      title: "Added instructions for arriving at the Vincit office",
      link: utils.event_url(event_2023_05_24.file),
      guid: "a856d240-c68c-40fd-8cbb-d843c058fbb3",
      description: [
        p([], [
          text(
            "To prevent the same problems we had last time, I've added some pictures to the arrival instructions on ",
          ),
          a([href(utils.event_url(event_2023_05_24.file))], [
            text("the event page"),
          ]),
          text("."),
        ]),
        p([], [
          text("I've also added a title for a new mini talk, "),
          em([], [text("MyProjectOS")]),
          text(". :) We still have other things planned also."),
        ]),
      ],
      pub_date: "Tue, 2 May 2023 21:40:00 +0200",
    ),
    Item(
      title: "Upcoming event: 24.5.2023",
      link: utils.event_url(event_2023_05_24.file),
      guid: "089899db-113b-47f4-ac2e-7c9f03f2b921",
      description: [
        p([], [
          text("TRElixir will be next held on the "),
          time([attribute("datetime", "2023-05-24")], [
            text("24th of May, 2023"),
          ]),
          text(
            ", at Vincit office in central Tampere, starting at 18.00 o'clock.",
          ),
        ]),
        p([], [
          a(
            [href(config.url <> "/" <> utils.event_url(event_2023_05_24.file))],
            [text("Go to event page.")],
          ),
        ]),
      ],
      pub_date: "Tue, 21 Feb 2023 21:30:00 +0200",
    ),
    Item(
      title: "Thanks and materials added",
      link: utils.event_url(event_2023_03_16.file),
      guid: "d11e88e3-87a1-4ee1-a02c-6e50b1c123d0",
      description: [
        p([], [
          text(
            "Thanks to everyone for attending the first TRElixir in a long time! It was really nice to get to see all of you. I've added my talk materials to the website.",
          ),
        ]),
        p([], [
          text(
            "The next TRElixir will likely be at the end of May or beginning of June. Watch this feed! :)",
          ),
        ]),
        p([], [
          text(
            "If you have inspiration to talk about something yourself at the next event, or ideas or feedback for the event, please let me know! My contact details are on the main page of the website.",
          ),
        ]),
      ],
      pub_date: "Sat, 18 Mar 2023 10:00:00 +0200",
    ),
    Item(
      title: "Lightning talk possibility",
      link: utils.event_url(event_2023_03_16.file),
      guid: "8f77b5c8-2de0-49bb-b652-077a601f762b",
      description: [
        text(
          "It's possible to hold a lightning talk (5–10 mins) at our event! Inquire for the possibility from Mikko Ahlroth (contact details on the website), or come talk to him before the event.",
        ),
      ],
      pub_date: "Sun, 12 Mar 2023 9:40:00 +0200",
    ),
    Item(
      title: "Upcoming event: 16.3.2023",
      link: utils.event_url(event_2023_03_16.file),
      guid: "cfc19ed7-f46d-45b6-a134-e11df93a864d",
      description: [
        p([], [
          text(
            "TRElixir will be next held on the 16th of March, 2023, at Vincit office in central Tampere, starting at 18.00 o'clock.",
          ),
        ]),
        p([], [
          a(
            [href(config.url <> "/" <> utils.event_url(event_2023_03_16.file))],
            [text("Go to event page.")],
          ),
        ]),
      ],
      pub_date: "Tue, 21 Feb 2023 21:30:00 +0200",
    ),
  ]
}
