import gleam/list
import gleam/string
import lustre/element.{type Element, element, text}
import lustre/attribute.{attribute}
import trelixir/feed/types.{type FeedItem}

pub fn view(items: List(FeedItem), site_url: String) {
  [
    element.advanced(
      "",
      "?xml version=\"1.0\" encoding=\"UTF-8\"?",
      [],
      [],
      False,
      True,
    ),
    element(
      "rss",
      [
        attribute("version", "2.0"),
        attribute("xmlns:dc", "http://purl.org/dc/elements/1.1/"),
      ],
      [
        element("channel", [], [
          element("title", [], [text("TRElixir")]),
          element.advanced("", "link", [], [text(site_url)], False, False),
          element("description", [], [text("TRElixir newsletter feed.")]),
          element("generator", [], [text("Lustre")]),
          element("copyright", [], [text("© Mikko Ahlroth")]),
          ..list.map(items, fn(item) {
            element("item", [], [
              element("title", [], [cdata([text(item.title)])]),
              element("dc:creator", [], [text("Mikko Ahlroth")]),
              element.advanced("", "link", [], [text(item.link)], False, False),
              element("guid", [], [text(item.guid)]),
              element("description", [], [cdata(item.description)]),
              element("pubDate", [], [text(item.pub_date)]),
            ])
          })
        ]),
      ],
    ),
  ]
}

fn cdata(content: List(Element(Nil))) {
  let content =
    content
    |> list.map(element.to_string)
    |> string.join("")

  element.advanced("", "![CDATA[" <> content <> "]]", [], [], False, True)
}
