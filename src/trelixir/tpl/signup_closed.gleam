import lustre/element/html.{a}
import lustre/element.{text}
import lustre/attribute.{class, href}

pub fn view() {
  [
    a([href(""), class("pure-button pure-button-disabled")], [
      text("Signup is closed"),
    ]),
  ]
}
