import gleam/set
import gleam/list
import lustre/element/html.{a, div, h2, h3, h4, img, li, p, section, ul}
import lustre/element.{text}
import lustre/attribute.{alt, attribute, class, href, src, target}
import trelixir/events/types

pub fn view(event: types.Event) {
  [
    section([class("event content contentpadding")], [
      h2([class("content-head is-center")], [text(event.title)]),
      div([class("pure-g")], [
        div(
          [class("l-box-lrg pure-u-1 pure-u-md-1-2")],
          list.append(event.description, [
            p([], [
              text("The event is graciously hosted by "),
              a([href(event.host_url)], [text(event.host_name)]),
              text("."),
            ]),
          ]),
        ),
        div([class("l-box-lrg pure-u-1 pure-u-md-1-2")], event.signup_info),
      ]),
    ]),
    section([class("event content contentpadding")], [
      h2([class("content-head is-center")], [text("Talks")]),
      div([class("pure-g")], {
        let #(_, content) =
          list.fold(event.talks, #(set.new(), []), fn(acc, talk) {
            let #(shown_speakers, prev_content) = acc
            let new_content =
              div([class("l-box-lrg pure-u-1 pure-u-md-1-2")], case talk {
                types.Placeholder -> [
                  h3([class("talk-title")], [text("More talks TBA")]),
                ]
                types.Talk(title: title, speaker: speaker, slides: slides) -> [
                  h3([class("talk-title")], [text(title)]),
                  case set.contains(shown_speakers, speaker) {
                    False ->
                      img([
                        src(speaker.img),
                        alt(speaker.name),
                        attribute("title", speaker.name),
                        class("speaker-image pure-img"),
                      ])
                    True -> element.none()
                  },
                  p([class("speaker-name")], [
                    text(speaker.name),
                    case
                      set.contains(shown_speakers, speaker),
                      speaker.company
                    {
                      False, types.Company(name: company) ->
                        text(", " <> company)
                      _, _ -> element.none()
                    },
                  ]),
                  case set.contains(shown_speakers, speaker) {
                    False ->
                      p([class("speaker-description")], speaker.description)
                    True -> element.none()
                  },
                  ..case slides {
                    [] -> [element.none()]
                    slides -> [
                      h4([], [text("Slides & materials")]),
                      ul(
                        [],
                        list.map(slides, fn(slide) {
                          li([], [
                            a([href(slide.link), target("_blank")], [
                              text(slide.title),
                            ]),
                          ])
                        }),
                      ),
                    ]
                  }
                ]
              })

            let new_speakers = case talk {
              types.Placeholder -> shown_speakers
              types.Talk(speaker: speaker, ..) ->
                set.insert(shown_speakers, speaker)
            }

            #(new_speakers, list.append(prev_content, [new_content]))
          })

        content
      }),
    ]),
  ]
}
