import gleam/list
import lustre/element/html.{a, div, h3, li, p, ul}
import lustre/element.{text}
import lustre/attribute.{class, href}
import trelixir/events/types

pub fn view(event: types.Event) {
  [
    h3([class("content-subhead")], [text(event.title)]),
    div([class("pure-g")], [
      div(
        [class("l-box-lrg pure-u-1 pure-u-md-1-2")],
        list.flatten([
          event.description,
          case event.talks {
            [] -> [element.none()]
            talks -> [
              p([], [text("Presentations:")]),
              ul(
                [],
                list.map(talks, fn(talk) {
                  let talk_title = case talk {
                    types.Placeholder -> "More talks TBA"
                    types.Talk(title: title, speaker: speaker, ..) ->
                      title <> " (" <> speaker.name <> ")"
                  }

                  li([], [text(talk_title)])
                }),
              ),
            ]
          },
        ]),
      ),
      div([class("l-box-lrg pure-u-1 pure-u-md-1-2 signupform")], [
        case event.file {
          "" -> element.none()
          file ->
            a([href(file <> ".html"), class("pure-button")], [
              text("Go to event page"),
            ])
        },
      ]),
    ]),
  ]
}
