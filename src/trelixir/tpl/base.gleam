import gleam/list
import lustre/element/html.{
  a, abbr, div, footer, h1, header, li, link, main, meta, p, section, ul,
}
import lustre/element.{type Element, text}
import lustre/attribute.{attribute, class, href, name, rel, type_}

pub fn view(is_index: Bool, title: String, content: List(Element(Nil))) {
  html.html([attribute("lang", "en")], [
    html.head([], [
      meta([attribute("charset", "utf-8")]),
      meta([
        name("viewport"),
        attribute("content", "width=device-width, initial-scale=1.0"),
      ]),
      html.title([], title <> " – TRElixir"),
      meta([
        name("description"),
        attribute(
          "content",
          "TRElixir is a developer meetup in Tampere focusing on the BEAM ecosystem.",
        ),
      ]),
      link([
        rel("alternate"),
        type_("application/rss+xml"),
        href("feed.xml"),
        attribute("title", "TRElixir RSS feed"),
      ]),
      link([
        rel("stylesheet"),
        href("https://unpkg.com/purecss@0.6.2/build/pure-min.css"),
        attribute(
          "integrity",
          "sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD",
        ),
        attribute("crossorigin", "anonymous"),
      ]),
      link([
        rel("stylesheet"),
        href("https://unpkg.com/purecss@0.6.2/build/grids-responsive-min.css"),
      ]),
      link([rel("stylesheet"), href("marketing.css")]),
    ]),
    html.body([], [
      header([class("header")], [
        div(
          [class("home-menu pure-menu pure-menu-horizontal pure-menu-fixed")],
          [
            case is_index {
              True ->
                a([class("pure-menu-heading"), href("#top")], [text("TRElixir")])
              False ->
                a([class("pure-menu-heading"), href("/")], [
                  text("⬅ TRElixir"),
                ])
            },
            case is_index {
              True ->
                ul([class("pure-menu-list")], [
                  menu_item("#event", "Events"),
                  menu_item("#contact", "Contact"),
                  menu_item("#sponsors", "Sponsored by"),
                  menu_item("#top", "Top"),
                ])
              False -> element.none()
            },
          ],
        ),
      ]),
      section(
        [
          class(
            "splash-container "
            <> case is_index {
              True -> ""
              False -> "event-splash"
            },
          ),
        ],
        [
          case is_index {
            True ->
              header([class("splash")], [
                h1([class("splash-head")], [text("TRElixir")]),
                p([class("splash-subhead")], [text("Tampere 💜 BEAM!")]),
                p([class("splash-subhead")], [
                  text("TRElixir is a developer meetup focusing on the "),
                  abbr([attribute("title", "Elixir, Erlang, Gleam, etc.")], [
                    text("BEAM"),
                  ]),
                  text(" ecosystem."),
                ]),
              ])
            False -> element.none()
          },
        ],
      ),
      main(
        [
          class(
            "content-wrapper "
            <> case is_index {
              True -> ""
              False -> "event-content"
            },
          ),
        ],
        list.append(content, [
          footer([class("footer l-box is-center")], [
            text(
              "Tampere background © Tampereen kaupunki, licensed under CC-BY-NC.",
            ),
          ]),
        ]),
      ),
    ]),
  ])
}

fn menu_item(url: String, content: String) {
  li([class("pure-menu-item")], [
    a([class("pure-menu-link"), href(url)], [text(content)]),
  ])
}
