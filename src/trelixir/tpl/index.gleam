import gleam/option
import gleam/list
import lustre/element/html.{a, div, h2, h3, img, kbd, p, section}
import lustre/element.{text}
import lustre/attribute.{
  alt, attribute, class, href, id, rel, src, target, type_,
}
import trelixir/events/types
import trelixir/tpl/index_event

pub fn view(
  upcoming_event: option.Option(types.Event),
  past_events: List(types.Event),
  fediverse_account: String,
  fediverse_url: String,
) {
  [
    section(
      [id("event"), class("ribbon l-box-lrg contentpadding")],
      list.flatten([
        [
          h2([class("content-head content-head-ribbon is-center")], [
            text("Upcoming event"),
          ]),
        ],
        case upcoming_event {
          option.Some(evt) -> index_event.view(evt)
          option.None -> [
            div([class("pure-g")], [
              div([class("l-box-lrg pure-u-1")], [
                p([], [
                  text(
                    "No upcoming event has yet been announced, but stay tuned!",
                  ),
                ]),
              ]),
            ]),
          ]
        },
        [
          h2([class("content-head content-head-ribbon is-center")], [
            text("Past events"),
          ]),
        ],
        list.flatten(list.map(past_events, fn(evt) { index_event.view(evt) })),
      ]),
    ),
    section([id("contact"), class("content contentpadding")], [
      h2([class("content-head is-center")], [text("Contact")]),
      div([class("pure-g")], [
        div([class("l-box-lrg pure-u-1 pure-u-md-1-2")], [
          h3([class("content-subhead")], [text("Posts")]),
          p([], [
            text("You can follow us on the fediverse at "),
            a([rel("me"), href(fediverse_url)], [text(fediverse_account)]),
            text(". You can also follow our "),
            a(
              [rel("alternate"), type_("application/rss+xml"), href("feed.xml")],
              [text("RSS feed")],
            ),
            text(
              " for the latest updates. We will post any upcoming meetups in both channels.",
            ),
          ]),
        ]),
        div([class("l-box-lrg pure-u-1 pure-u-md-1-2")], [
          h3([class("content-subhead")], [text("IM")]),
          p([], [
            text("Currently the most relevant discussion can be found on the "),
            a([href("https://koodiklinikka.fi")], [
              text("Koodiklinikka.fi Slack server"),
            ]),
            text(", on the channel "),
            kbd([], [text("#elixir")]),
            text("."),
          ]),
          p([], [
            text("You can also shoot Mikko Ahlroth a message "),
            a([href("https://social.ahlcode.fi/@nicd")], [
              text("on the fediverse"),
            ]),
            text(", on IRC (Nicd @ Libera.chat), or "),
            a([href("mailto:trelixir@nytsoi.net")], [text("by email")]),
            text("."),
          ]),
        ]),
      ]),
    ]),
    section([id("sponsors"), class("ribbon l-box-lrg contentpadding")], [
      div([class("l-box-lrg pure-u-1 sponsorlogos")], [
        a([href("https://vincit.com/"), target("_blank")], [
          img([
            class("pure-img"),
            src("vincitlogo.jpg"),
            alt("Vincit"),
            attribute("title", "Vincit"),
          ]),
        ]),
      ]),
    ]),
  ]
}
