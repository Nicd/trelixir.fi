import gleam/list
import gleam/string
import gleam/io
import lustre/element.{type Element}
import lustre/ssg
import trelixir/index
import trelixir/feed/feed
import trelixir/tpl/feed as feed_tpl
import trelixir/tpl/page
import trelixir/tpl/base
import trelixir/events/event_2023_05_24
import trelixir/events/event_2023_03_16
import trelixir/events/event_2017_09_21
import trelixir/config

pub fn main() {
  let site =
    ssg.new("./dist")
    |> ssg.add_static_dir("./assets")

  let index_content = base.view(True, "TRElixir", index.render())
  let site = ssg.add_static_route(site, "/", index_content)

  let feed_items = feed.items()
  let feed_content = feed_tpl.view(feed_items, config.url)
  let site = ssg.add_static_asset(site, "/feed.xml", render(feed_content))

  let events = [
    #(event_2023_05_24.file, event_2023_05_24.data()),
    #(event_2023_03_16.file, event_2023_03_16.data()),
    #(event_2017_09_21.file, event_2017_09_21.data()),
  ]

  let site =
    list.fold(events, site, fn(acc, event) {
      let #(file_name, data) = event
      let event_content = base.view(False, data.title, page.view(data))
      ssg.add_static_route(acc, "/" <> file_name, event_content)
    })

  io.debug(ssg.build(site))
}

fn render(content: List(Element(Nil))) {
  content
  |> list.map(element.to_string)
  |> string.join("")
}
